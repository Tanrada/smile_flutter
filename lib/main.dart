import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Smile :) ';

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      title: _title,
        theme: ThemeData(
        primaryColor: Color(0xff232f34),
        accentColor: Color(0xffF9AA32),
        // primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      
      home: Scaffold(
        appBar: AppBar(title: const Text(_title), actions: <Widget>[
           IconButton(
                    icon: Icon(Icons.poll),
                    color: Colors.white,
                    onPressed: () {
                      print('test button');
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.archive),
                    color: Colors.white,
                    onPressed: () {
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.print),
                    color: Colors.white,
                    onPressed: () {
                    },
                  )
  ],),

        
        body: MyStatelessWidget(),

      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Name',
            style: TextStyle(fontStyle: FontStyle.italic),
            
          ),
        ),
        DataColumn(
          label: Text(
            'ID',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Tier',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'LTV',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Total Trans.',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Total Point',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Remaining Point',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[           
            DataCell(Text('Bussaracum')),
            DataCell(Text('082 466 9224')),
            DataCell(Text('tttt')),
            DataCell(Text('500,008')),
            DataCell(Text('1')),
            DataCell(Text('40,000')),
            DataCell(Text('40,000')),

          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Leelar')),
            DataCell(Text('087 436 8466')),
            DataCell(Text('tttt')),
            DataCell(Text('10,000')),
            DataCell(Text('1')),
            DataCell(Text('800')),
            DataCell(Text('800')),

          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Papawarin M.')),
            DataCell(Text('090 084 3048')),
            DataCell(Text('tttt')),
            DataCell(Text('111,050')),
            DataCell(Text('1')),
            DataCell(Text('4,442')),
            DataCell(Text('4,442')),

          ],
        ), 
        DataRow(
          cells: <DataCell>[
            DataCell(Text('VeryCust')),
            DataCell(Text('019 990 9999')),
            DataCell(Text('tttt')),
            DataCell(Text('8,600')),
            DataCell(Text('1')),
            DataCell(Text('2,064')),
            DataCell(Text('2,064')),

          ],
        ),
      ],
    );
  }
  


}
